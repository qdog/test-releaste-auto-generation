# v0.5.0
## Features
* Resolve "add a label to restrict the issues included in the changelog" [!40](https://gitlab.com/qdog/test-releaste-auto-generation/-/merge_requests/40)
* Resolve "add on stop for environment" [!36](https://gitlab.com/qdog/test-releaste-auto-generation/-/merge_requests/36)
* Resolve "create environment for staging and production" [!35](https://gitlab.com/qdog/test-releaste-auto-generation/-/merge_requests/35)

# v0.4.0
## Features
* Resolve "Make the main branch auto update the change log." [!29](https://gitlab.com/qdog/test-releaste-auto-generation/-/merge_requests/29)

## Bug Fixes
* Resolve "fix the versioning issue when loading form changelogemanager" [!33](https://gitlab.com/qdog/test-releaste-auto-generation/-/merge_requests/33)
* Resolve "fix the pipeline for before script variable" [!31](https://gitlab.com/qdog/test-releaste-auto-generation/-/merge_requests/31)
* Resolve "reverse the rules in gitlab ci" [!30](https://gitlab.com/qdog/test-releaste-auto-generation/-/merge_requests/30)

# v0.3.0
## Features
* Resolve "gitlab pipeline for running the script when merges happen with main branch" [!24](https://gitlab.com/qdog/test-releaste-auto-generation/-/merge_requests/24)
* make the script change CHANGELOG.md [!23](https://gitlab.com/qdog/test-releaste-auto-generation/-/merge_requests/23)
* automaticly create a new release and tag with the script. [!22](https://gitlab.com/qdog/test-releaste-auto-generation/-/merge_requests/22)

## Bug Fixes
* Resolve "actually fix the pipeline" [!25](https://gitlab.com/qdog/test-releaste-auto-generation/-/merge_requests/25)
