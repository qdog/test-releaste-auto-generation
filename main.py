from typing import Optional
import argparse
import json
import collections
import gitlab
from typing import TypedDict
from enum import Enum
import re
import os

parser = argparse.ArgumentParser()
parser.add_argument('--url', type=str, default='https://gitlab.com')
parser.add_argument('--personal-token', type=str, default=None)
parser.add_argument('--job-token', type=str, default=None)
parser.add_argument('--project-id', type=int, required=True)
parser.add_argument('--inclusion-label', type=str, default=None)
parser.add_argument('--remove-resolve-title', action='store_true')
parser.add_argument('--create-release', action='store_true')

version_parser = parser.add_argument_group(title="versioning")
version_parser.add_argument('--patch-labels', nargs='+')
version_parser.add_argument('--patch-title', type=str, default='Bug Fixes')

version_parser.add_argument('--minor-labels', nargs='+')
version_parser.add_argument('--minor-title', type=str, default='Features')

version_parser.add_argument('--major-labels', nargs='+')
version_parser.add_argument('--major-title', type=str, default='Major Release')

args = parser.parse_args()

if (not args.create_release) and \
        any(i is None for i in [args.patch_labels, args.minor_labels, args.major_labels]):
    parser.error('when not creating a release via --create-release, you need to specifiy the '
            '--patch-labels, --minor-labels and --major-labels to update the CHANGELOG.md')

class SemVarSections(str, Enum):
    MAJOR = args.major_title
    MINOR = args.minor_title
    PATCH = args.patch_title


SEMVAR_LABELS: dict[SemVarSections, list[str]] = {
    SemVarSections.MAJOR: args.major_labels,
    SemVarSections.MINOR: args.minor_labels,
    SemVarSections.PATCH: args.patch_labels,
}


class Record(TypedDict):
    title: str
    url: str
    reference: str


class Output(TypedDict):
    version: tuple[int, int, int]
    commit: str
    records: dict[SemVarSections, list[Record]]


gl = gitlab.Gitlab(args.url, private_token=args.personal_token, job_token=args.job_token)

project = gl.projects.get(args.project_id)

class Parser:
    def __init__(
            self,
            project: gitlab.v4.objects.Project,
            remove_resolve_title: bool = False,
            inclusion_label: Optional[str] = None 
            ):

        merge_requests = project.mergerequests.list(
                state='merged', 
                target_branch=project.default_branch,
                )
        merge_requests.sort(key=lambda x: x.merged_at, reverse=True)
        
        latest_merge_request = merge_requests[0]
        latest_releases = project.releases.list(order_by='released_at', sort='desc')

        if len(latest_releases)>0:
            latest_release_date = latest_releases[0].released_at
            assert re.match("^v(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)", latest_releases[0].tag_name) is not None
            current_release_version: tuple[int, int, int] = tuple(int(i) for i in latest_releases[0].tag_name[1:].split('.'))
        else:
            latest_release_date = '0'


        needed_merge_requests = []
        for m in merge_requests:
            if m.merged_at<latest_release_date:
                continue
            if inclusion_label is not None and inclusion_label not in m.labels:
                continue
            needed_merge_requests.append(m)
        print(f"{len(needed_merge_requests)=}")
        records: dict[SemVarSections, list[Record]] = {
            SemVarSections.MAJOR: [],
            SemVarSections.MINOR: [],
            SemVarSections.PATCH: [],
        }

        for m in needed_merge_requests:
            for label in m.labels:
                title = m.title
                if remove_resolve_title and title.startswith('Resolve '):
                    title = title[9:-1]

                r = Record(title=title, url=m.web_url, reference=m.reference)
                for s in SemVarSections:
                    if label in SEMVAR_LABELS[s]:
                        records[s].append(r)

        new_version = list(current_release_version)
        if len(records[SemVarSections.MAJOR])>0:
            new_version[0] += 1
            new_version[1] = new_version[2] = 0
        elif len(records[SemVarSections.MINOR])>0:
            new_version[1] += 1
            new_version[2] = 0
        elif len(records[SemVarSections.PATCH]):
            new_version[2] += 1
        
        if latest_merge_request.squash:
            commit = latest_merge_request.squash_commit_sha
        else:
            commit = latest_merge_request.sha

        self._output = Output(version=new_version, commit=commit, records=records)
    
    def get_output(self) -> Output:
        return self._output


class CreateMarkdown:
    def __init__(
            self,
            output: Output,
            ):
        
        self.output = output 

    def get_markdown(self, include_version: bool = True) -> str:
        
        sections: list[str] = []
        for semv, records in self.output['records'].items():
            if len(records)==0:
                continue
            section = f"## {semv}\n"
            section += self.parse_records(records)
            section += '\n'
            sections.append(section)
        
        sections_str = '\n'.join(sections)
        
        result = ""
        if include_version:
            result += f"# v{'.'.join(map(str,self.output['version']))}\n"

        result += f"{sections_str}"

        return result


    def parse_records(self, records: list[Record]) -> str:
        rows: list[str] = []
        for r in records:
            rows.append(f"* {r['title']} [{r['reference']}]({r['url']})")
        return '\n'.join(rows)


class CreateRelease:
    def __init__(
            self,
            project: gitlab.v4.objects.Project,
            version: tuple[int, int, int],
            commit: str,
            release_notes: str,
            ):
        
        self.project = project
        self.tag_name=f"v{version[0]}.{version[1]}.{version[2]}"
        self.commit = commit
        self.release_notes = release_notes
    
    def create_tag(self):
        self.project.tags.create(
                    dict(tag_name=self.tag_name, ref=self.commit, message=f"version {self.tag_name} release")
                )

    def create_release(self):
        self.create_tag()
        self.project.releases.create(
            dict(
                    tag_name=self.tag_name,
                    description=self.release_notes,
                )
        )


class ChangelogManager:
    def __init__(self, path: str = "CHANGELOG.md"):
        self.path = path
        if os.path.exists(self.path):
            with open(self.path,"r") as fp:
                raw = fp.read()
        else:
            raw = ""
        self.delimiter = "\n# "
        self.splited = raw.split(self.delimiter)

    def get_last_changelog(self, include_version: bool = False) -> str:
        v = self.splited[0]
        if not include_version:
            v.split('\n', 1)[1]
        return v

    def get_last_version(self, splited: bool = False) -> str:
        v = self.splited[0].split('\n', 1)[0].split('# ')[1]
        if splited:
            v = tuple(int(i) for i in v[1:].split('.'))
            assert len(v)==3
        return v

    def add_changelog(self, changelog: str, overwrite_last: bool = False):
        if overwrite_last:
            self.splited[0] = changelog
        else:
            self.splited[0] = self.splited[0][2:]
            self.splited = [changelog] + self.splited

    def flush(self):
        with open(self.path, "w") as fp:
            raw = self.delimiter.join(self.splited)
            fp.write(raw)


def check_tag_exist(tag: str) -> bool:
    try:
        project.tags.get(tag)
        return True
    except gitlab.exceptions.GitlabGetError:
        return False 


def create_release():
    changeloger = ChangelogManager()
    default_branch = project.branches.get(project.default_branch)
    releaser = CreateRelease(
            project=project,
            version=changeloger.get_last_version(splited=True),
            commit=default_branch.commit['id'],
            release_notes=changeloger.get_last_changelog(include_version=False),
    )

    releaser.create_release()


def update_changelog():
    p = Parser(project=project,
            remove_resolve_title=args.remove_resolve_title,
            inclusion_label=args.inclusion_label)
    output = p.get_output()
    if all(len(i)==0 for i in output['records'].values()):
        return 

    markdowner = CreateMarkdown(output=output)
    
    changeloger = ChangelogManager()
    overwrite_last = not check_tag_exist(changeloger.get_last_version())
    changeloger.add_changelog(markdowner.get_markdown(), overwrite_last=overwrite_last)
    changeloger.flush()


def main():
    if args.create_release:
        create_release()
    else:
        update_changelog()


if __name__ == "__main__":
    raise SystemExit(main())


